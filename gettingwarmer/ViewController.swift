//
//  ViewController.swift
//  gettingwarmer
//
//  Copyright (c) 2015 iAchieved.it LLC. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
  
  override func viewDidLoad() {
    super.viewDidLoad()
    // Do any additional setup after loading the view, typically from a nib.
  }

  override func didReceiveMemoryWarning() {
    super.didReceiveMemoryWarning()
    // Dispose of any resources that can be recreated.
  }
  
  override func viewDidAppear(animated: Bool) {
    super.viewDidAppear(animated)
    
    NSNotificationCenter.defaultCenter().addObserver(self, selector: "locationDenied", name: "LOCATION_DENIED", object: nil)
    let beaconManager = BeaconManager.sharedInstance
  }

  func locationDenied() {
    let alert = UIAlertController(title:"Permission Required", message:"Location services permission is required.", preferredStyle:.Alert)
  
    let ok = UIAlertAction(title: "OK", style: .Default) { (action) -> Void in
    }
  
    alert.addAction(ok)
    self.presentViewController(alert, animated:true){}
  }


}

