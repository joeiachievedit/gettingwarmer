//
//  BeaconManager.swift
//  gettingwarmer
//
//  Copyright (c) 2015 iAchieved.it LLC. All rights reserved.
//

import Foundation
import CoreLocation

class BeaconManager : NSObject, CLLocationManagerDelegate {
  
  static let sharedInstance = BeaconManager()
  
  var locationManager:CLLocationManager
  
  override init() {
    self.locationManager = CLLocationManager()
    
    super.init()
    
    self.locationManager.delegate = self
    self.locationManager.requestWhenInUseAuthorization()
    
  }
  
  func startRanging() {
    
    // NOTE:  The UUIDString here must match the UUID of your iBeacon.  If your
    //        iBeacon UUID is different, replace the string below accordingly!
    let uuid   = NSUUID(UUIDString:"788FA98B-871C-4C71-9944-88ADEC84A8DA")
    let region = CLBeaconRegion(proximityUUID: uuid, identifier: "")
    self.locationManager.startRangingBeaconsInRegion(region)
    
  }
  
  // MARK:  CLLocationManagerDelegate methods
  func locationManager(manager: CLLocationManager!,
    didChangeAuthorizationStatus status: CLAuthorizationStatus) {
      switch status {
      case .NotDetermined:
        self.locationManager.requestWhenInUseAuthorization()
      case .AuthorizedWhenInUse, .AuthorizedAlways:
        self.startRanging()
      case .Denied, .Restricted:
        NSNotificationCenter.defaultCenter().postNotificationName("LOCATION_DENIED", object: nil)
      default:
        break
      }
  }
  
  func locationManager(manager: CLLocationManager!,
    didRangeBeacons beacons: [AnyObject]!,
    inRegion region: CLBeaconRegion!) {
      
      let beaconsRanged = beacons as! [CLBeacon]!
      
      if let beacon = beaconsRanged.last {
        println("Beacon ranged:  \(beacon)")
      }
  }
  
  
}