Companion source code to series on iBeacon:

* Part One - http://dev.iachieved.it/iachievedit/getting-started-with-ibeacon/
* Part Two - http://dev.iachieved.it/iachievedit/writing-an-ibeacon-app-in-swift/